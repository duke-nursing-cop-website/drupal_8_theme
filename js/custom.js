// Custom Jquery code for the website
  // Set banner height
FontAwesome.config.searchPseudoElements = true

jQuery.noConflict()(document).ready(function( $ ) {
  const resizeBannerImages = () => {
    if ($('.slogan').length === 0) {
      return;
    }
    const slogan_height =  $('.slogan .last-line').height() + $('.slogan .last-line').position().top + 20;
    $('.slogan').height(slogan_height);
	  $('.banner img').each((index, value) => {
		  $(value).height(slogan_height);
    });
  }

  resizeBannerImages();
  $(window).resize( () => {
    resizeBannerImages();
  });

  const openCloseMenu = () => {
    $(".hamburger-menu").toggleClass("open");
    $(".overlay").toggleClass("active");
    $("#menu-container").toggleClass("active");
    fixTopMenuWidth($("#menu-container .navbar-dark"));
  }

  const fixTopMenuWidth = myObj => {
    const parentWidth = myObj.parent().width();
    myObj.css("width", parentWidth);
  };

  $(document.body).on('click touchend', '.accordion-toggle .menuitem, .go-back', (event) => {
    const parentTarget = $(event.currentTarget).closest('.accordion-toggle');
    const submenu = parentTarget.find(".accordion-content");
    submenu.toggleClass("active");
    parentTarget.find(".topmenu").toggleClass("fixed");
    $(".accordion-toggle")
      .not(parentTarget)
      .toggleClass("d-none");
    $(".go-back").toggleClass("d-none");
    fixTopMenuWidth(parentTarget.find(".topmenu"));
    return false;
  });

  $(".accordion-toggle a ").on('click', () => {
    openCloseMenu();
    return false;
  });

  $(window).on("orientationchange", function(event) {
    $("#menu-container").removeClass("active");
    $(".accordion-toggle .topmenu")
      .removeClass("d-none")
      .css("width", "");
    $("#menu-container .navbar-dark").css("width", "");
    $(".hamburger-menu").removeClass("open");
    $(".overlay").removeClass("active");
  });

  $(document.body).on('click', '.dismiss' , function(event) {
    openCloseMenu();
    return false;
  });

  $(document.body).on('click touchend', '.open-menu', function(event) {
    openCloseMenu();
    return false;
  });

  function ajaxLink(selector, url, context, dochangestate) {
    var scriptsIncluded = drupalSettings.data.ajax_links_api.scripts_included;
    if (scriptsIncluded === 0) {
      selectorNew = context;
    }
    else {
      selectorNew = selector;
    }

    $.ajax({
      url: url,
      type: 'GET',
      data: 'ajax=1',
      success: function (data) {
        ajaxAfter(selector, url, data, window, document, scriptsIncluded, dochangestate);
        Drupal.attachBehaviors(selectorNew);
      },
      error: function (xhr) {
        var data = xhr.response.replace('?ajax=1', '');
        ajaxAfter(selector, url, data, window, document, scriptsIncluded, dochangestate);
      }
    });
  }

  function ajaxAfter(selector, url, data, window, document, scriptsIncluded, dochangestate) {
    if (scriptsIncluded === 0) {
      ajaxLoading = false;
    }
    $(selector).css('height', '');
    $(selector).html(data);

    $('a.active').removeClass('active').parents('li').removeClass('active-trail');
    $('a').filter(function () {
      return url.endsWith($(this).attr('href'));
    }).addClass('active').parents('li').addClass('active-trail');

    var html5 = drupalSettings.data.ajax_links_api.html5;
    if (html5 === 1 && window.history.replaceState) {
      var matches = data.match('<title>(.*?)</title>');
      if (matches) {
        title = $('<div/>').html(matches[1]).text();
        document.title = title;
      }
      if (dochangestate) {
        window.history.replaceState({page: 0}, document.title, window.location.href);
        window.history.pushState({page: 1}, document.title, url);
      }
    }

    var viewsPager = drupalSettings.data.ajax_links_api.vpager;
    if (viewsPager === 1) {
      $(selector + ' .view .pager a').each(function () {
        var href = $(this).attr('href');
        href = href.replace('?ajax=1', '');
        href = href.replace('&ajax=1', '');
        $(this).attr('href', href);
      });
    }

  }

  function ajaxBefore(selector) {
    $(selector).css('height', $(selector).height() + 'px');
    $(selector).html('<div class="ajax-links-api-loading"></div>');
  }

  $(document).on('change', '.categories-select select', (event) => {
    const url = event.currentTarget.value;
    const selector = drupalSettings.data.ajax_links_api.selector;
    ajaxBefore(selector);
    ajaxLink(selector, url, null, true);
  });

  $(document).on('submit', '.search.form-inline', (event) => {
    const overlayElement = $('.overlay');
    if (overlayElement.length == 1 && overlayElement.hasClass('active')) {
      openCloseMenu();
    }
    const url = '/clinical-module-search/' + encodeURIComponent(event.currentTarget[0].value);
    const selector = drupalSettings.data.ajax_links_api.selector;
    ajaxBefore(selector);
    ajaxLink(selector, url, null, true);
    return false;
  });
});
