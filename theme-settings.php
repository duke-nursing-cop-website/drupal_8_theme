<?php

function copdukenursing_8_form_system_theme_settings_alter(&$form, Drupal\Core\Form\FormStateInterface $form_state) {
  $form['copdukenursing_8_settings']['general']['become_a_preceptor_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Become a Preceptor URL'),
    '#description' => t('Link to the Become a Preceptor Form'),
    '#default_value' => theme_get_setting('become_a_preceptor_url')
  );
  $form['copdukenursing_8_settings']['general']['clinical_module_url'] = array(
    '#type' => 'textfield',
    '#title' => t('CoP Clinical Module URL'),
    '#description' => t('Link to the CoP Clinical Module Home Page'),
    '#default_value' => theme_get_setting('clinical_module_url')
  );
  $form['copdukenursing_8_settings']['general']['clinical_module_title'] = array(
    '#type' => 'textfield',
    '#title' => t('CoP Clinical Module Title'),
    '#description' => t('Phrase Synonymous with Clinical Modules'),
    '#default_value' => theme_get_setting('clinical_module_title')
  );
  $form['copdukenursing_8_settings']['general']['facebook_url'] = array(
    '#type' => 'textfield',
    '#title' => t('CoP Nursing FaceBook URL'),
    '#description' => t('Link to the CoP Nursing FaceBook Site'),
    '#default_value' => theme_get_setting('facebook_url')
  );
  $form['copdukenursing_8_settings']['general']['twitter_url'] = array(
    '#type' => 'textfield',
    '#title' => t('CoP Nursing Twitter URL'),
    '#description' => t('Link to the CoP Nursing Twitter Site'),
    '#default_value' => theme_get_setting('twitter_url')
  );
  $form['copdukenursing_8_settings']['general']['linkedin_url'] = array(
    '#type' => 'textfield',
    '#title' => t('CoP Nursing LinkedIn URL'),
    '#description' => t('Link to the CoP Nursing LinkedIn Site'),
    '#default_value' => theme_get_setting('linkedin_url')
  );
}